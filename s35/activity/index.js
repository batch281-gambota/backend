const express = require('express')
const mongoose = require('mongoose')
const app = express()
const port = 4000;


mongoose.connect('mongodb+srv://dennielgambota:8VEcehPj9yHTrGgR@wdc028-course-booking.2e91qjf.mongodb.net/b281-to-do?retryWrites=true&w=majority',{
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"))
db.once("open", () => console.log(`We're connected to the database`));

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

const User = mongoose.model("Users", userSchema)


app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.post("/signup", (req, res) => {
	User.findOne({username : req.body.name}).then((result, err) => {
		if(result !== null && result.name === req.body.name){
			return res.send(`Duplicate task found`);
		} else {
			let newUser = new User({
				username : req.body.username,
				password : req.body.password,
			})

			newUser.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else {
					return res.status(201).send("New User created");
				}
			})
		}
	})
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}