const express = require('express')
const mongoose = require('mongoose')
const app = express()
const port = 4000;



mongoose.connect('mongodb+srv://dennielgambota:8VEcehPj9yHTrGgR@wdc028-course-booking.2e91qjf.mongodb.net/b281-to-do?retryWrites=true&w=majority',{
	useNewUrlParser : true,
	useUnifiedTopology : true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"))

db.once("open", () => console.log(`We're connected to the database`));

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default	: "pending"
	}
})

// parameters - collection, taskSchema
const Task = mongoose.model("Tasks", taskSchema);

app.post("/tasks", (req, res) => {

	Task.findOne({name : req.body.name}).then((result, err) => {
		if(result !== null && result.name === req.body.name){
			return res.send(`Duplicate task found`);
		} else {
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr)
				}
				else {
					return res.status(201).send("New task created");
				}
			})
		}
	})	
})

app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}