// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

let trainer = {

    // Properties
    name: "Ash",
    age: 18,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"], 
        kanto: ["Brock", "Misty"]
    },

    // Methods
    talk: function(){
        console.log("Pikachu! I choose you!");
    }
}
console.log(trainer);




// Check if all properties and methods were properly added


// Access object properties using dot notation
console.log("Result of dot notation: ");
console.log(trainer.name);

// Access object properties using square bracket notation
console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log("Result of talk method ");
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 5 * level;
    this.attack = 3 * level;

    this.tackle = (target) => {
        console.log(this.name + ' tackled '+ target.name);
        console.log(target.name + " health is now reduced to " + Number(target.health -= this.attack));
        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = () => console.log(this.name + ' fainted');
}

// Create/instantiate a new pokemon
let kadabra = new Pokemon("Kadabra", 7);
console.log(kadabra);

// Create/instantiate a new pokemon
let gyarados = new Pokemon("Gyarados", 10);
console.log(gyarados);

// Create/instantiate a new pokemon
let tepig = new Pokemon("Tepig", 18);
console.log(tepig);

// Invoke the tackle method and target a different object
kadabra.tackle(gyarados);
console.log(gyarados);

// Invoke the tackle method and target a different object
tepig.tackle(kadabra);
console.log(kadabra )







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}