let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current Array");
console.log(fruits);


let fruitsLength = fruits.push("Mango");
console.log("Mutated array from push method");
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log("Mutated array from push method");
console.log(fruits);

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// add elements at the beginning of array

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// remove elements at the beginning of array

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

fruits.splice(2,3, "Calamansi", "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);


fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);


fruits.reverse();
console.log("Mutated array from sort method");
console.log(fruits);

let countries = ["US", "PH", "CAN", "SG", "PH", "FR", "GE", "CH", "KR"];

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf Method: " + firstIndex);
console.log(countries);


let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf Method: " + invalidCountry);
console.log(countries);


let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf Method: " + lastIndex);
console.log(countries);


let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf Method: " + lastIndexStart);
console.log(countries);

// slice elements from an array and returns a new array

//from start
let slicedArrayA = countries.slice(1);
console.log("Result forms slice method");
console.log(slicedArrayA);

// starting index to last index
let slicedArrayB = countries.slice(2, 6);
console.log("Result forms slice method");
console.log(slicedArrayB);

// from the last item
let slicedArrayC = countries.slice(-5);
console.log("Result forms slice method");
console.log(slicedArrayC);


let stringArray = countries.toString();
console.log(stringArray);


let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method");
console.log(tasks);


let tasks2 = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result from concat method");
console.log(tasks2);


let tasks3 = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method");
console.log(tasks3);


let users = ["John", "Juan", "Dudong"];
console.log(users.join("-"));


/*
        - Iteration methods are loops designed to perform repetitive tasks on arrays
        - Iteration methods loops over all items in an array.
        - Useful for manipulating array data resulting in complex tasks
        - Array iteration methods normally work with a function supplied as an argument
        - How these function works is by performing tasks that are pre-defined within an array's method.
    */

// forEach()
    /*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
		- It's common practice to use the singular form of the array content for parameter names used in array loops
        - forEach() does not return anything.
        - Syntax
            arrayName.forEach(function(indivElement) {
                statement
            })
    */

tasks.forEach(function(task){
	console.log(task);
})


filteredTasks = [];	
tasks2.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})
console.log(filteredTasks);



/* 
        - Iterates on each element AND returns new array with different values depending on the result of the function's operation
        - This is useful for performing tasks where mutating/changing the elements are required
        - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
        - Syntax
            let/const resultArray = arrayName.map(function(indivElement))
    */

let numbers = [1, 2, 3, 4, 5];

let numbermap = numbers.map((number) => {
	return number * number;
});
console.log("Original Array");
console.log(numbers);
console.log("Result of map method");
console.log(numbermap);

let numberForEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numberForEach);