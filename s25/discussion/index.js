// JSON 


// JSON as objects
// {
// 	"city": "Quezon city",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// // JSON Arrays
// "cities": [
// 	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"
// 	},
// 	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"
// 	},
// 	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"
// 	}

// ]

// "dogs": [
// 	{"name": "Browny", "age": "4", "breed": "pitbull"},
// 	{"name": "Whity", "age": "3", "breed": "shitzu"},
// 	{"name": "Blacky", "age": "5", "breed": "poodle"}
// ]

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X', batchName: 'Batch Y' }];

// the "stringify" method is used to conver JavaScript objects into a string

console.log('Result from stringify method: ');
console.log(JSON.stringify(batchesArr));

// Using Stringify method with variables
// User details
// let firstName = prompt('What is your first name');
// let lastName = prompt('What is your last name');
// let age = prompt('What is your age?');
// let address = {
// 	city: prompt('Which city do you live in?'),
// 	country: prompt('Which country does your city belong to?')
// }
// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

// Mini Activity

// let car = JSON.stringify({
// 	brand: prompt('Enter car brand: '),
// 	type: prompt('Enter car type: '),
// 	year: prompt('Enter car year: ')
// })

// console.log(car);

// Converting Stringified JSON into JavaScript Objects
let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log(`Result from parse method`);
console.log(JSON.parse(batchesJSON));
