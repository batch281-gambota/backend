const http = require("http");

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

const port = 4000;

let app = http.createServer((request, response) => {
	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	if(request.url == "/users" && request.method == "POST"){
		// requestBody acts as a placeholder for the resource/data to be created later on
		let requestBody = '';

		// Stream is a sequence of data
		request.on('data', (data) => {
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		request.on('end', () => {
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Creates a new object reprensenting the new mock database
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new uset to the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
});

app.listen(port, () => console.log(`Server running at localhost: ${port}`));