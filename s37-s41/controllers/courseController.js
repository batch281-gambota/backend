const Course = require('../models/Course');
// const auth = require("../auth");
// const userRoutes = require('../routes/userRoutes')


// module.exports.addCourse = (reqBody, userData) => {
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	})
	

// 	if (userData.isAdmin) {

// 		return newCourse.save().then((course, error) => {
// 			if(error) {
// 				return false;
// 			} else {
// 				return "new course added";
// 			}
// 		})

// 	} else {
// 		return "no access";
// 	}
// }

module.exports.addCourse = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveCourse = (reqParams, reqBody) => {
	let updatedStatus = {isActive : reqBody.isActive};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedStatus).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}