const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');


// router.post("/", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
// })

router.post("/", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

router.get('/all', (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


router.get('/', (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

router.get('/:courseId', (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// auth.verify to check if user is logged in
router.put('/:courseId', auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

router.patch('/:courseId', auth.verify, (req, res) => {
	const data = {
		isActive: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

module.exports = router;