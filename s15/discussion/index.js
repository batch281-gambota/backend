// Comments

// Comments are parts of the code that gets ignored by the language
//Comments are meant to describe the written code

/* 
	Two types of comments
	1. Single line
	2. Multi line
*/
console.log("Hello, World!");

// Variables
/*
	- it is used to contain data
*/
// Declaring Varaiables - tells our devices that a variable name is created and is ready to store data

/*
	Syntax:
		let/const variableName;

	let - keyword usually used in declaring a variable
	const - keyword usually used in declaring CONSTANT variable
*/

let myVariable;
console.log()

// Initializing variables - the instance whena a variable is given it's initial/starting value.

/*
	Syntax:
		let/const variableName = value;
*/
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
// Syntax: variableName = newValue;

productName = 'laptop';
console.log(productName);


// numbers
// integers
let  headcount = 32;
console.log(headcount);

// deciman numbers/frantions
let grade = 98.7
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining text/number and strings
console.log("John's grade last quarter is " + grade);


// Boolean - used to store valued relating to the state of certain things

let isMarried = true;
let inGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Array - are special kind of data type that's used to store multiple values;
// Syntax
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

/*
	Syntax: 
		let/const onjectName = {
			propertyA : value,
			propertyB : value
		}
*/

// Object

let person = {
	fullName : 'Juan Dela Cruz',
	age : 35,
	isMarried : false,
	contact : ["0917 123 4567", "8123 4567"],
	address: {
		houseNumber : '345',
		city : 'Manila'
	}
}
console.log(person);

let myGrades = {
	firstGrading : 98.7,
	secondGrading : 92.1,
	thirdGrading : 90.2,
	fourthGrading : 94.6
}
console.log(myGrades);

console.log(typeof myGrades);
console.log(typeof grades);