const express = require('express');
const mongoose = require('mongoose');

const cors = require("cors")
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
const cartRoutes = require("./routes/cartRoutes")
const uploadRoutes = require("./routes/uploadRoutes")



const app = express();

mongoose.connect(`${process.env.MONGO_DB}/Capstone_2?retryWrites=true&w=majority`,
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error"))
db.once('open', () => console.log('Now connected to MongoDB Atlas!'))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)
app.use("/cart", cartRoutes)
app.use("/upload", uploadRoutes)


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})