const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required :[true, "Firstname is required."]
	},
	lastName : {
		type: String,
		required :[true, "Lastname is required."]
	},
	email : {
		type: String,
		required :[true, "Email is required."]
	},
	mobileNo : {
		type: Number,
		required :[true, "Mobile is required."]
	},
	password : {
		type : String,
		required: [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false	
	},
	cart : [
		{
			productId : {
				type: String,
				required: true
			}, 
			quantity : {
				type: Number,
				required: true
			},
			subtotal : {
				type: Number,
				required: true
			}
		}
	]
	// history : [
	// 	{
	// 		transactionDate: {
	// 			type: Date,
	// 			default: new Date()
	// 		},
	// 		remarks : {
	// 			type: String,
	// 			default: ""
	// 		}
	// 	}
	// ]

})
module.exports = mongoose.model("User", userSchema);