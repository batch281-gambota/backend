const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	userId : {
		type: String,
		required: true
	},
	products : [
		{
			productId : {
				type: String,
				required: true
			},
			quantity : {
				type: Number,
				required: true
			}

			// productStatus : {
			// 	type: String,
			// 	default: "Pending"
			// },
			// remarks : {
			// 	type: String,
			// 	default: "Awaiting Update"
			// }

		}
	],
	totalAmount : {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}

	// orderStatus: {
	// 	type: String,
	// 	default: "Being Processed"
	// },
	// history : [
	// 	{
	// 		transactionDate: {
	// 			type: Date,
	// 			default: new Date()
	// 		},
	// 		description : {
	// 			type: String,
	// 			default: ""
	// 		}
	// 	}
	// ]
})
module.exports = mongoose.model("Order", orderSchema);