const cloudinary = require("cloudinary").v2

const cloudinaryConfig = cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET,
  secure: true
})

module.exports.getSignature = () => {

	const timestamp = Math.round(new Date().getTime() / 1000)
	const signature = cloudinary.utils.api_sign_request(
	  {
	    timestamp: timestamp
	  },
	  cloudinaryConfig.api_secret
	)
	return ({ timestamp, signature })
}

module.exports.doSomething = () => {
	// based on the public_id and the version that the (potentially malicious) user is submitting...
	// we can combine those values along with our SECRET key to see what we would expect the signature to be if it was innocent / valid / actually coming from Cloudinary
	const expectedSignature = cloudinary.utils.api_sign_request({ public_id: req.body.public_id, version: req.body.version }, cloudinaryConfig.api_secret)

	// We can trust the visitor's data if their signature is what we'd expect it to be...
	// Because without the SECRET key there's no way for someone to know what the signature should be...
	if (expectedSignature === req.body.signature) {
	  // Do whatever you need to do with the public_id for the photo
	  // Store it in a database or pass it to another service etc...
	  
	}
}

module.exports.deleteImg = () => {
	// actually delete the photo from cloudinary
	cloudinary.uploader.destroy(req.body.id)
}