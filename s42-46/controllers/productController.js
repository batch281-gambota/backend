const Product = require("../models/Product");
const auth = require("../auth");

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		imgId: reqBody.imgId,
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		quantity : reqBody.quantity,
		createdOn: Date("<YYYY-mm-ddTHH:MM:ssZ>")
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return "Error adding product.";
		} else {
			return true;
		}
	})
}

module.exports.searchAllProducts = (reqBody) => {
	return Product.find({$or: [ {name: { $regex: reqBody, $options: 'i'}}, {description: { $regex: reqBody, $options: 'i'}} ]}).sort({"name": 1}).collation({locale: "en", caseLevel: true}).then(result => {
		if(result == "" || result == undefined){
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getAllProducts = (sort) => {
	switch(sort){
		case "nameAscend":
			return Product.find({ "name": { "$exists": true } }).sort({"name": 1}).collation({locale: "en", caseLevel: true}).then(result => {
				if(result == ""){
					return 'No Products Available';
				} else {
					return result;
				}
			})
			break;
		case "nameDescend":
			return Product.find({ "name": { "$exists": true } }).sort({"name": -1}).collation({locale: "en", caseLevel: true}).then(result => {
				if(result == ""){
					return 'No Products Available';
				} else {
					return result;
				}
			})
			break;
		default:
			return Product.find({}).then(result => {
				if(result == ""){
					return 'No Products Available';
				} else {
					return result;
				}
			})

		}
	
}

module.exports.searchActiveProduct = (reqBody) => {
	return Product.find({isActive: true, $or: [ {name: { $regex: reqBody.name, $options: 'i'}}, {description: { $regex: reqBody.name, $options: 'i'}} ] }).then(result => {
		if(result == undefined){
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).sort({name: 1}).then(result => {
		if(result == undefined){
			return false;
		} else {
			return result;
		}
	})
}

module.exports.searchSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		imgId: reqBody.imgId,
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		quantity : reqBody.quantity
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {
	let updatedStatus = {isActive : false};

	return Product.findByIdAndUpdate(reqParams.productId, updatedStatus).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.activateProduct = (reqParams) => {
	let updatedStatus = {isActive : true};

	return Product.findByIdAndUpdate(reqParams.productId, updatedStatus).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

