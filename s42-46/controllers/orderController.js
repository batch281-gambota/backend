const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");

module.exports.checkout = async (data) => {
	let total = 0;
	let newOrder = new Order({
		userId : data.userId,
		products : [],
		totalAmount : total,
		purchasedOn: data.purchasedOn
	})

	return await User.findById(data.userId).then(async user => {
		newOrder.products = user.cart;
		let checkStock;
		if(user.cart == ""){
			return {'status': 'empty'};
		}
		for (const item of user.cart) {
			checkStock = await Product.findById(item.productId).then((res, error) => {
				if(item.quantity > res.quantity || res.quantity == 0){
					return {'status': 'check'};
				} else {
					total += item.subtotal;
					newOrder.totalAmount = total;
					res.quantity -= item.quantity;
					res.save();
					return true;
				}
				
			})
			if(checkStock != true)
			{return(checkStock);}
		}
		if(checkStock){
			return newOrder.save().then((order, error) => {
				if(error){
					return false;
				} else {
					user.cart = [];
					user.save();
					return true;
				}
			})	
		}
	})


}

module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	})
}

module.exports.getUserOrders = (userInfo) => {
	return Order.find({userId: userInfo.id}).then(result => {
		if(result == ""){
			return 'You have not placed any orders yet. Check our products to make one.';
		} else {
			return result;
		}
	})
}

