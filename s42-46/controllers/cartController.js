const User = require("../models/User");
const Product = require("../models/Product");


module.exports.getUserCart = (userInfo) => {

	return User.findById(userInfo.id).then(async user => {
		let checkQuantity, getTotal = true, updateQuantity;
		if(user.cart == "" || user.cart == null){
			return false;
		} else {
			let total = 0;

			// iterate on each product on user cart [array] and check if item on cart has enough stock
			for(const prod of user.cart){

				// Assign Max stock left 
				checkQuantity = await Product.findById(prod.productId).then((res, error) => {
					if(prod.quantity > res.quantity){
						prod.quantity = res.quantity;
					} 
				})

				// Update totalamount
				// getTotal = await Product.findById(prod.productId).then((res, error) => {
				// 	if(!error){
				// 		prod.subtotal = prod.quantity * res.price;
				// 		total += res.price * prod.quantity
				// 		return true;
				// 	}
				// })
			}
			if(getTotal){
				
				return user.save().then((user, error) => {
					if(error){
						return false;
					}
					return user.cart;
				})
			}
		}
	})

}

module.exports.updateUserCart = (data) => {

	return User.findOne({_id: data.userId}).then(async user => {
		if(user.cart == ""){
			return `Your cart is Empty. There is no Item to update quantity.`
		}
		for(const prod of user.cart){
			if(data.productId == prod.productId){
				prod.quantity = data.quantity;
				prod.subtotal = prod.quantity * data.price;
				user.save()
				return prod;
			} 
		}
	})
}

module.exports.addToCart = (data) => {
	
	return User.findById(data.userId).then(async user => {

		let updateSubTotal;
		let updatedCart = [];

		// Check if item/s already exists in user cart; if true: sum up quantity and update subtotal
		for (const input of data.cart) {
			for (const retrieved of user.cart){
				if(input.productId == retrieved.productId){
					input.quantity += retrieved.quantity;
					user.cart = user.cart.filter(item => item !== retrieved)
				} 
			}
			data.cart = data.cart.filter(item => item !== input)

			// Update subtotal per item on cart
			updateSubTotal = await Product.findById(input.productId).then((res, error) => {
				input.subtotal = input.quantity * res.price;
				updatedCart.push(input);
				return true; 
			})
		}

		if(updateSubTotal){
			// Assign summed up existing items with back; Assign new items on cart; Assign existing items back but not summed up/no match on new cart
			user.cart = updatedCart.concat(data.cart, user.cart);

			return user.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})
		}
		
	})
}

module.exports.removeItemFromCart = (data) => {

	return User.findById(data.userId).then(async user => {
		if(user.cart == ""){
			return `Cannot remove more Item. Your cart is Empty.`
		}

		user.cart = user.cart.filter(item => item.productId !== data.productId)
		user.save()
		return {'status': 'success'}
		
		
	})
}