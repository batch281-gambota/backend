const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmail = async(reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0){
			return true;
		} else {
			return false;
		}
	})
}

module.exports.updatePassword = (reqParams, reqBody) => {
	return User.findByIdAndUpdate(reqParams.userId, {password: bcrypt.hashSync(reqBody.password, 10)}).then((user, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.registerUser = async (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email : reqBody.email,
		mobileNo: reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.getAllAccount = (req) => {
	return User.find({}).then(result => {
		if(result == ""){
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then(result => {

		if(result == null ){
			return false;
		} else {
			result.password = "";
			return result;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		
		if(result == null ){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}

		}
	})
}

module.exports.getUserOrders = async (reqParams) => {
	return findUserOrders = await Order.find({userId : reqParams.userId}).then(result => {
		if(result == null){
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getUserDetails = async (reqParams) => {
	return findUserDetails = await User.findById(reqParams.userId).then(result => {
		if(result == null){
			return false;
		} else {
			userDetails = result;
			result.password = "";
			return userDetails;
		}
	})
}

module.exports.setAsAdmin = (reqParams, reqBody) => {
	
	return User.findByIdAndUpdate(reqParams.userId, {isAdmin: reqBody.isAdmin}).then((user, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}