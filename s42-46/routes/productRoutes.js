const express = require('express');
const auth = require('../auth.js')
const router = express.Router();

const productController = require("../controllers/productController");


router.post("/", auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	if(userInfo.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return false;
	}
})

router.post("/searchAll", (req, res) => {
	productController.searchAllProducts(req.body.data).then(resultFromController => res.send(resultFromController))
})

router.post("/sort", (req, res) => {
	productController.getAllProducts(req.body.data).then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	productController.getActiveProducts(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/active", (req, res) => {
	productController.searchActiveProduct(req.body).then(resultFromController => res.send(resultFromController))
})

router.get('/:productId', (req, res) => {
	productController.searchSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
})


router.put('/:productId/update', auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);
	
	if(userInfo.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('You need to be an Admin for this request.');
	}
})

router.put('/:productId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('You need to be an Admin to archive product.');
	}
})


router.put('/:productId/activate', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('You need to be an Admin to activate product.');
	}
})

module.exports = router;