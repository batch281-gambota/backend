const express = require('express');
const auth = require('../auth.js')
const router = express.Router();

const cartController = require("../controllers/cartController");

router.patch('/', auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);
	const data = {
		userId: userInfo.id,
		cart: req.body.cart
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController))
})

router.post('/', auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	cartController.getUserCart(userInfo).then(resultFromController => res.send(resultFromController))
})

router.put('/', (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	const data = {
		userId: userInfo.id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price
	}

	cartController.updateUserCart(data).then(resultFromController => res.send(resultFromController))
})

router.delete('/', (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	const data = {
		userId: userInfo.id,
		productId: req.body.productId
	}
	cartController.removeItemFromCart(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;