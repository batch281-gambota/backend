const express = require('express');
const router = express.Router();
const auth = require('../auth.js')

const userController = require("../controllers/userController");

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.getAllAccount(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return false;
	}
})


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/:userId/orders", auth.verify, (req, res) => {
	userController.getUserOrders(req.params).then(resultFromController => res.send(resultFromController))
})

router.post("/:userId", auth.verify, (req, res) => {
	userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController))
})

router.patch("/:userId/reset", auth.verify, (req, res) => {
	userController.updatePassword(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	if(userInfo.isAdmin){
		userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('You need to be an Admin for this request.');
	}
	
})

module.exports = router;