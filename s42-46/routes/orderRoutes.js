const express = require('express');
const auth = require('../auth.js')
const router = express.Router();

const orderController = require("../controllers/orderController");

router.post('/checkout', auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);
	const data = {
		userId: userInfo.id,
		products: req.body.products,
		totalAmount: req.body.totalAmount,
		purchasedOn: Date("<YYYY-mm-ddTHH:MM:ssZ>")
	}
	if(userInfo.isAdmin){
		return false;
	}

	if(!userInfo.isAdmin){
		orderController.checkout(data).then(resultFromController => res.send(resultFromController))
	}
})

router.get("/all", auth.verify, (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	if(userInfo.isAdmin){
		orderController.getAllOrders(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("You need to be an Admin for this request.");
	}
})

router.get('/', (req, res) => {
	const userInfo = auth.decode(req.headers.authorization);

	if(userInfo.isAdmin) res.send("You cannot checkout items on Cart as Admin.")

	if(!userInfo.isAdmin){
		orderController.getUserOrders(userInfo).then(resultFromController => res.send(resultFromController))
	}
})

module.exports = router;