let numA = -1;

if(numA < 0){
	console.log("The number is less than 0");
}
console.log(numA < 0);

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

let numB = 1;
 
if(numA < 0){
	console.log("Hello");
} else if(numB > 0){
	console.log("World");
}

city = "Tokyo"
if(city === "New York"){
	console.log("Welcome to New York City!");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

function checkHeight(input){
	if(Number(input) <= 150){
		console.log("Did not pass the min height requirement.");
	}else{
		console.log("Pass the min height requirement.");
	}
}

// checkHeight(prompt("Enter your height:"));



// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
  };


let name;

function isOfLegalAge(){
	name = "John";
	return "You are of legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit";
}

// let yourAge = Number(prompt("What is your age?"));
// console.log(yourAge);

// let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();

// console.log("result of ternary operator in function: " + legalAge + ", " + name);


let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break;

	case 'tuesday':
		console.log("The color of the day is orange");
		break;

	case 'wednesday':
		console.log("The color of the day is yellow");
		break;

	case 'thursday':
		console.log("The color of the day is green");
		break;

	case 'friday':
		console.log("The color of the day is blue");
		break;

	case 'saturday':
		console.log("The color of the day is indigo");
		break;

	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("please input a valid day of the week");
		break;
}	