// 1. Add and Subtract
function addNumbers(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(Number(num1) + Number(num2));
}

addNumbers(prompt("Enter first number: "), prompt("Enter second number: "));

function subtractNumbers(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(Number(num1) - Number(num2));
}

subtractNumbers(prompt("Enter first number: "), prompt("Enter second number: "));

// 2. Multiply and Divide

function multiplyNumbers(num1, num2){
	console.log("Displayed product of " + num1 + " and " + num2);
	return Number(num1) * Number(num2);
}

function divideNumbers(num1, num2){
	console.log("Displayed quotient of " + num1 + " and " + num2);
	return Number(num1) / Number(num2);
}


let product = multiplyNumbers(prompt("Enter first number: "), prompt("Enter second number: "));
console.log(product);
let quotient = divideNumbers(prompt("Enter first number: "), prompt("Enter second number: "));
console.log(quotient);



// 3. Area of Circle

function calcCircleArea(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius");
	return area = 3.14 * (radius ** 2);
}

let circleArea = calcCircleArea(prompt("Enter circle radius"));


console.log(circleArea);

// 4. Average of four numbers

function aveOfNumbers(num1, num2, num3, num4){
	return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = aveOfNumbers(50, 60, 70, 80);
console.log("The average of 50, 60, 70 and 80:");
console.log(averageVar);

// 5. 
function checkIsPassingScore(score, total){
	let scorePercentage = (score / total) * 100;
	let isPassed = scorePercentage > 75;
	console.log("Is " + score + "/" + total + " a passing score?");
	return isPassed;
}

let isPassingScore = checkIsPassingScore(prompt("Enter Score"), prompt("Enter Total"));

console.log(isPassingScore);
