const express = require('express')
const app = express()
const port = 4000;

app.use(express.json())

app.use(express.urlencoded({extended:true}))


app.get("/", (req, res) => {
	res.send('Hello World');
})

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint");
})

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})


let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)

		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input both username and password")
	}
})

app.put("/change-password", (req, res) => {
	let message;

	for(let i = 0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			break;
		}else{
			app.send('user not found');
		}
	}
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}