let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined];

let tasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
	];

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [city1, city2, city3];

console.log(tasks);
console.log(cities);

console.log("Array length");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Aizaac Estiva";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[3];
console.log("Accessing arrays using variables");
console.log(currentLaker);


console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);


let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);